from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class ForumUnitTest(TestCase):
    def test_forum_url_is_exist(self):
        response = Client().get('/company/profile/')
        self.assertEqual(response.status_code, 200)

    def test_forum_using_index_func(self):
        found = resolve('/company/profile/')
        self.assertEqual(found.func, index)
