from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Todo

import os

# Create your views here.
response = {}

def add_forum(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        todo = Todo(description=response['description'])
        todo.save()
        return HttpResponseRedirect('/company/forum/')
    else:
        return HttpResponseRedirect('/company/forum/')

def index(request):
    forum_list = Todo.objects.all()

    page = request.GET.get('page', 1)
    paginate_data = paginate_page(page, forum_list)
    forum = paginate_data['data']
    page_range = paginate_data['page_range']

    response = {"todo": forum, "todo_form": Todo_Form , "page_range" : page_range}
    html = 'forum/forum.html'
    return render(request, html, response)

def paginate_page(page, data_list):
    paginator = Paginator(data_list, 5)

    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    # Get the index of the current page
    index = data.number - 1

    # maximum page
    max_index = len(paginator.page_range)

    # range of 10, slicing
    start_index = index if index >= 10 else 0
    end_index = 10 if index < max_index - 10 else max_index

    # Get our new page range. In the latest versions of Django page_range returns 
    # an iterator. Thus pass it to list, to make our slice possible again.
    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data':data, 'page_range':page_range}
    return paginate_data
