from django import forms

class Todo_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    description_attrs = {
        'type': 'text',
        'cols': 48,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Masukkan pernyataan...'
    }

    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
