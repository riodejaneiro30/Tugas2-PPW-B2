from django.db import models

# Create your models here.
class Todo(models.Model):
    # title = models.CharField(max_length=27)
    description = models.TextField()
    nomor = models.IntegerField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)

