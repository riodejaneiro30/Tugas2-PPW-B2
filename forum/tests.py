from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_forum, paginate_page
from .models import Todo
from .forms import Todo_Form
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models.manager import Manager
from unittest.mock import patch

# Create your tests here.
class ForumUnitTest(TestCase):
    def test_forum_url_is_exist(self):
        response = Client().get('/company/forum/')
        self.assertEqual(response.status_code, 200)

    def test_forum_using_index_func(self):
        found = resolve('/company/forum/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_todo(self):
        new_activity = Todo.objects.create(description='deadline besok')
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_forum_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/company/forum/add_forum', {'description': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/company/forum/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_forum_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/company/forum/add_forum', {'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/company/forum/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_invalid_page(self):
        response = Client().get('/company/forum/?page=*')
        self.assertRaises(PageNotAnInteger)

    def test_empty_page(self):
        response = Client().get('company/forum/?page=-9')
        self.assertRaises(EmptyPage)

    def test_invalid_page_pagination_number(self):
        data = ["tes1", "coba1", "yaudalahya", "tes1", "coba1"]
        test1 = paginate_page("...", data)
        test2 = paginate_page(-1, data)
        with patch.object(Manager, 'get_or_create') as a:
            a.side_effect = PageNotAnInteger("page number is not an integer")
            res = paginate_page("...", data)
            self.assertEqual(res['page_range'], test1['page_range'])
        with patch.object(Manager, 'get_or_create') as a:
            a.side_effect = EmptyPage("page number is less than 1")
            res = paginate_page(-1, data)
            self.assertEqual(res['page_range'], test2['page_range'])

