// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Use the API call wrapper to request the member's profile data
function getProfileData() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);
}

// Handle the successful return from the API call
function displayProfileData(data){
    var user = data.values[0];
    $("#name").append('Logged in as '+user.first-name+' '+user.last-name);
    document.getElementById("picture").innerHTML = '<img src="'+user.pictureUrl+'" />';
    document.getElementById("name").innerHTML = user.firstName+' '+user.lastName;
    document.getElementById("intro").innerHTML = user.headline;
    document.getElementById("email").innerHTML = user.emailAddress;
    document.getElementById("location").innerHTML = user.location.name;
    document.getElementById("link").innerHTML = '<a href="'+user.publicProfileUrl+'" target="_blank">Visit profile user</a>';
    document.getElementById('profileData').style.display = 'block';
    
    // get company data
    var url = "/companies?format=json&is-company-admin=true";
    IN.API.Raw(url)
    .method("GET")
    .result(displayCompanyData)
    .error(onError);
}



function displayCompanyData(data){
		cpnyID = data.values[0].id; 
        IN.API.Raw("/companies/" + cpnyID + ":(id,name,ticker,description)?format=json")
        .method("GET")
        .result(displayCompanyData)
        .error(onError);
        saveUserData(user);
        for(i in data.values){
			console.log(data);
            document.getElementById("companyLists").innerHTML = data.values[i].name + " " + data.values[i].id;
        }
    }

    // Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Destroy the session of linkedin
function logout(){
    IN.User.logout(removeProfileData);
}

// Remove profile data from page
function removeProfileData(){
    document.getElementById('profileData').remove();
}

function saveUserData(userData){
    $.post('saveUserData.php', {oauth_provider:'linkedin',userData: JSON.stringify(userData)}, function(data){ return true; });
}


// $("#commentButton").onClick(function(){
//     inputText = $("textBoxNya").val();
//     console.log($(this).val());
//     console.log("test");
//     $.ajax({
//         method: "POST",
//         url: '{% url ":add_comment"%}',
//         data: {
//             'comment': inputText,
//         },
//         dataType : 'json',
//         success: function(data){
//             data = JSON.parse(data)
//             html = '<div class="comment">' + "<h1>" + data.comment + "Post nya keluar" + '</h1>'+"</div>";
//             console.log(data)
//             $("#postnya").append(html)
//         }

//     });
// });