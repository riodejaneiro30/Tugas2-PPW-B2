from django.db import models
from forum.models import Todo
class Komentar(models.Model):
    forum = models.ForeignKey(Todo, null=True)
    nama = models.CharField('nama', max_length=400, default='DEFAULT VALUE')
    isi = models.CharField('isi', max_length=400, default='DEFAULT VALUE')