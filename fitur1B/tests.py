from django.test import TestCase, Client
from django.urls import resolve
from .models import Komentar
from forum.models import Todo
from django.http import HttpResponseRedirect, HttpResponse
from .views import index,kumpulkan_komentar,add_comment


class AppsOneTest(TestCase):
    def test_fitur_1B_url_is_exist(self):
        response = Client().get('/fitur1B/')
        self.assertEqual(response.status_code, 200)
    def test_fitur_1B_using_index_func(self):
        found = resolve('/fitur1B/')
        self.assertEqual(found.func, index)
    
    # def test_kumpul_komentar(self):
    #     #Creating a new activity
    #     forum = Forum.objects.create(pengguna=pengguna, isi='tes', nomor=1)
    #     komentar = Komentar.objects.create(forum=forum, isi='tesKomentar')

    #     forum = Forum.objects.filter(pengguna = pengguna)

    #     komentar = []

    #     for i in forum:
    #         a = Komentar.objects.filter(forum = i)
    #         for j in a:
    #             komentar.append(j)

    #     hasil = kumpulkan_komentar(forum)

    #     self.assertEqual(komentar, hasil)


    def test_post_buat_komentar(self):
    	forum = Todo.objects.create(description='tes', nomor=1)
    	response = self.client.post('/fitur1B/add_comment/', {'isi' : 'contoh', 'nomor' : 1, 'nama':'hehe'})
    	self.assertEqual(response.status_code, 200)

    def test_post_buat_komentar_gagal(self):
        response = Client().get('/fitur1B/add_comment/', {'isi' : '', 'nomor' : 1})
        self.assertEqual(response.status_code, 403)

