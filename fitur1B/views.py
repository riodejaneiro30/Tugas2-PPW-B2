from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Komentar
from forum.models import Todo
import json

# Create your views here
response = {'author': 'Yesica AsikBanget Company'}
def index(request):
    # html = 'fitur1B/fitur1B.html'
    # return render(request, html, response)
    forum1 = Todo.objects.all()
    komentar = kumpulkan_komentar(forum1)
    html = 'fitur1B/fitur1B.html'
    response['gambar'] = 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTc0NzU2OTYyN15BMl5BanBnXkFtZTgwMTkwOTg2MTI@._V1_SX300.jpg'
    response['forum'] = forum1
    response['komentar'] = komentar
    page = request.GET.get('page', 1)
    paginator = Paginator(forum1, 3)
    navigasi = paginator.page(page)
    response['navigasi'] = navigasi
    return render(request,html,response)


# def model_to_dict(obj):
#     data = serializers.serialize('json',[obj,])
#     struct = json.loads(data)
#     data = json.dumps(struct[0]["fields"])
#     return data

# @csrf_exempt
# def add_comment (request,pk):
#     status = Reply.objects.get(pk=pk)
#     form = Comment_Form(request.POST or None)
#     if(request.method == 'POST' and form.is_valid()):
#         response['comment'] = request.POST['comment']
#         comment = Comment(comment=response['comment'])
#         comment.status = status
#         comment.save()
#         data = model_to_dict (comment)
#         return HttpResponse(data)
#     else:
#         return HttpResponseRedirect('/')

# def comment_post(request, post):
#     query = Update.objects.get(pk=post)
#     response['comment_form'] = Comment_Form
#     if(request.method == 'POST' and form.is_valid()):
#         response['comment'] = request.POST['comment']
#         comment = Update(message=response['comment'])
#         comment.save()
#         return HttpResponse('/comment')
#     else:
#         return HttpResponseRedirect('/comment')

def kumpulkan_komentar(forum):
    komentar = []

    for i in forum:
        a = Komentar.objects.filter(forum = i)
        for j in a:
            komentar.append(j)

    return komentar

@csrf_exempt
def add_comment(request):
    if request.method == 'POST':
        isi = request.POST['isi']
        nomor = request.POST['nomor']

        forum = Todo.objects.get(nomor = nomor)

        komentar = Komentar()
        komentar.forum = forum
        komentar.isi = isi
        komentar.nama = request.POST['nama']
        komentar.save()

        ambil = Komentar.objects.filter(isi = isi)

        return HttpResponse(status=200)
    else:
        return HttpResponse(status=403)
