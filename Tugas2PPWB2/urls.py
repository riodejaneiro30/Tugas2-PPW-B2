"""Tugas2PPWB2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import forum.urls as forum
import fitur1B.urls as fitur1B
import fitur2.urls as fitur2
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^company/forum/', include(forum,namespace='forum')),
    url(r'^fitur1B/', include(fitur1B,namespace='fitur1B')),
    url(r'^company/profile/', include(fitur2,namespace='fitur2')),
    url(r'^$', RedirectView.as_view(url="/fitur1B/", permanent='true'),name='index')
]
